#!/usr/bin/env python
 
from time import sleep
from time import gmtime, strftime
import datetime
import subprocess
import os
import glob
import httplib, urllib
import RPi.GPIO as GPIO
import random
import time
import threading
import picamera
import pygame
from pygame.locals import *
from pygame import mixer
import peewee
from peewee import *
import sys
import getopt

#Args
skipPushover = False
skipPushoverUnas = False
skipPushoverMujeril = False
try:
	opts, argv = getopt.getopt(sys.argv[1:], "s:")
	for opt, arg in opts:
		if opt == '-s':
			if arg == 'pushover':
				skipPushover = True
				print("Skipping all pushover messages")
			elif arg == 'unas':
				skipPushoverUnas = True
				print("Skipping all pushover messages to Unas")
			elif arg == 'mujeril':
				skipPushoverMujeril = True
				print("Skipping all pushover messages to Mujeril")
except:
	print("Error: Could not get args")


GPIO.setmode(GPIO.BCM)
GPIO.setup(6, GPIO.IN)   # Green button
GPIO.setup(19, GPIO.IN)	 # Red button
GPIO.setup(22, GPIO.IN)	 # Blue button
GPIO.setup(18, GPIO.IN)  # Doorbell
GPIO.setup(5, GPIO.OUT)  # Green LED
GPIO.setup(26, GPIO.OUT) # Red LED
GPIO.setup(27, GPIO.OUT) # Blue LED
GPIO.setup(25, GPIO.OUT) # IR LED

class FuncThread(threading.Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        threading.Thread.__init__(self)
 
    def run(self):
        self._target(*self._args)

class Doorbell(Model):
	id = PrimaryKeyField(primary_key=True)
	date = DateTimeField(default=datetime.datetime.now)
	test = BooleanField(default=False)
	class Meta:
		database = MySQLDatabase('doorbell', host='10.0.1.5', port=3306, user='doorbell',passwd='doorbell')

class Picture(Model):
        id = PrimaryKeyField(primary_key=True)
        # Apparently peewee searches from picture table with "_id" at the end, so "doorbell_id" need to be here "doorbell"
        doorbell = ForeignKeyField(Doorbell, to_field="id")
        file = BlobField()
        class Meta:
                database = MySQLDatabase('doorbell', host='10.0.1.5', port=3306, user='doorbell',passwd='doorbell')


def SaveToDatabase(testing = False):
	doorbellObj = Doorbell(test = testing)
	doorbellObj.save()

	# Saving the pictures as well
	pictureAmount = 3
	sleepTime = 2
	path = '/home/pi/doorbell/pictemp/'
        for i in range(pictureAmount):
		filepath = path + str(i) + '.jpg'
                camera.capture(filepath)
		# Saving picture to database
		with open(filepath, "rb") as imageFile:
			b = bytearray(imageFile.read())
	
		picture = Picture(doorbell = doorbellObj.id, file = b)
		picture.save()
		# Deleting temporary picture file
		os.remove(filepath)
		
		# If this is the last picture, then no need to sleep
		if (i != (pictureAmount - 1)): 
				sleep(sleepTime)

def PushOver(title,message):
	app_unas_key = 'abAgxoiTFiGP5RSMi4PKW7coPkApy6'
	user_unas_key = 'uaYJDNFkBXaqzJgsgPdC4ubUQYqER6'
	user_mujeril_key = 'uefxJ6WMGzp3wW99YYBjMBtgEXm5Zf'
	app_mujeril_key = 'a5XL9Y1r5YWDgQb9NgbXudP1mBHRdZ'
	#Connect with the Pushover API server
	conn_unas = httplib.HTTPSConnection("api.pushover.net:443")
	conn_mujeril = httplib.HTTPSConnection("api.pushover.net:443")

	if (skipPushover == False):
		#Send a POST request in urlencoded json to unas
		if (skipPushoverUnas == False):
			conn_unas.request("POST", "/1/messages.json",
				urllib.urlencode({
					"token": app_unas_key,
					"user": user_unas_key,
					"message": message
				}), { "Content-type": "application/x-www-form-urlencoded" })
			#Any error messages or other responses?
			conn_unas.getresponse()
		
		if (skipPushoverMujeril == False):
			#Send a POST request in urlencoded json to mujeril
			conn_mujeril.request("POST", "/1/messages.json",
				urllib.urlencode({
					"token": app_mujeril_key,
					"user": user_mujeril_key,
					"message": message
				}), { "Content-type": "application/x-www-form-urlencoded" })
			#Any error messages or other responses?
			conn_mujeril.getresponse()

def ShowImage(imageFile, stop_event):
	print 'In image'
	#img = pygame.image.load(imageFile)
	#white = (255, 64, 64)
	#w = 360
	#h = 240
	#screen = pygame.display.set_mode((w, h))
	#screen.fill((white))
	#running = 1

	#while (not stop_event.is_set()):
	#	screen.fill((white))
	#	screen.blit(img,(0,0))
	#	pygame.display.flip()
	#pygame.quit()
	#print 'Exiting image'

# Setting camera
camera = picamera.PiCamera()
camera.resolution = (800, 480)
camera.framerate = 30
camera.start_preview()
camera.hflip = True
camera.vflip = True
camera.led = False

# Getting files from the christmas folder
files = []
for file in glob.glob('/home/pi/doorbell/audio/christmas/*.mp3'):
	files.append(file)

# Our player
musicPlaying = False
buttonPressAmount = 0
musicFile = 'default.mp3'
mixer.init()

# For fun
red = False
blue = False
# Turning all led's off, incase they were left on earlier (ctrl-c)
GPIO.output(26, False) # Red
GPIO.output(27, False) # Blue
GPIO.output(5, False)  # Green

# This variable tells us when the green button was first pressed. It will be later be compared to current time, and if 2 seconds has elapsed, then asking if user want's to restart
restartTimer = 0
testTimer = 0
# Tell's us if the doorbell needs to be tested
testDoorbell = False

# Making sure, that the table exists
if (Doorbell.table_exists() == False): # Or, create_table(True), will not give an exception
	Doorbell.create_table()
if (Picture.table_exists() == False):
	Picture.create_table()

print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'Doorbell started!\r'

try:
	while True:
		if (GPIO.input(19) == False): # Red button pressed
			if (red == False):
				GPIO.output(26, True)
				GPIO.output(25, True) # Turning IR LED on
				red = True
				print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'red on\r'
			else:
				GPIO.output(26, False)
				GPIO.output(25, False) # IR LED off
				red = False
				print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'red off\r'
			sleep(0.5)
		elif (GPIO.input(22) == False): # Blue button pressed
			if (blue == False):
				# If user is holding down the button, then they want to test the doorbell
				while (GPIO.input(22) == False):
					if (GPIO.input(22) == False and testTimer == 0):
						testTimer = time.time()
					elif (GPIO.input(22) == False and (testTimer + 2) <= time.time()):
						# Asking user if they want to test the doorbell
						# Turning red and green led's on
						GPIO.output(26, True)
						GPIO.output(5, True)
						testImageThread_stop = threading.Event()
						camera.stop_preview()
						testImageThread = FuncThread(ShowImage, '/home/pi/doorbell/image/testDoorbell.jpg', testImageThread_stop)
						testImageThread.start();
						while True:
							if (GPIO.input(19) == False): # Red
								testTimer = 0
								GPIO.output(5, False) # Green off
								sleep(0.5)
								GPIO.output(26, False) # Red off
								testImageThread_stop.set()
								camera.start_preview()
								break
							elif (GPIO.input(6) == False): # Green
								testTimer = 0
								testDoorbell = True
								GPIO.output(26, False) # Red off
								sleep(0.5)
								GPIO.output(5, False) # Green off
								testImageThread_stop.set()
								camera.start_preview()
								break
						break
				if (testDoorbell == False): # If doorbell was released sooner than 2 seconds, then mute doorbell
					testTimer = 0
					GPIO.output(27, True)
					blue = True
					print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'blue on\r'
			else:
				GPIO.output(27, False)
				blue = False
				print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'blue off\r'
			sleep(0.5)
		# If green button pressed while music not playing
		elif (GPIO.input(6) == False and musicPlaying == False):
			if (restartTimer == 0): # If restartTimer hasn't been set, then the user just now started pressing the button
				restartTimer = time.time()
			elif ((restartTimer + 2) <= time.time()): # When 2 seconds has passed, asking user if they want to restart
				greenDown = True
				# Turning red and green led's on
				GPIO.output(26, True)
				GPIO.output(5, True)
				
				while greenDown: # Waiting for user to let go of green
					if (GPIO.input(6) == True): # If green is no longer being pressed
						greenDown = False
				# Waiting for user to select green of red
				greenPressed = False
				# Setting picture asking if we should really restart
				restartImageThread_stop = threading.Event()
				camera.stop_preview()
				restartImageThread = FuncThread(ShowImage, '/home/pi/doorbell/image/restart.jpg', restartImageThread_stop)
				restartImageThread.start();
				while True:
					if (GPIO.input(19) == False): # Red
						GPIO.output(5, False) # Green off
						sleep(0.5)
						GPIO.output(26, False) # Red off
						restartImageThread_stop.set()
						camera.start_preview()
						break
					elif (GPIO.input(6) == False): # Green
						GPIO.output(26, False) # Red off
						sleep(0.5)
						GPIO.output(5, False) # Green off
						restartImageThread_stop.set()
						camera.start_preview()
						os.system('reboot')
						greenPressed = True
				if (greenPressed == True):
					break
		else:
			restartTimer = 0
			
			
		# If someone rang the doorbell or if user wanted to test the doorbell
		if (GPIO.input(18) == True and musicPlaying == False or testDoorbell == True):
			
			# Saving to the database, pictures are handled here aswell
			databaseThread = FuncThread(SaveToDatabase, testDoorbell)
			databaseThread.start()
			testDoorbell = False
			
			# Checking if it's christmas time
			if(time.strftime("%m") == "12" or time.strftime("%m") == "01" and time.strftime("%d") <= "06"):
				rand = random.choice(files) # Selecting random christmas song
				#p = vlc.MediaPlayer("file://" + rand)
                                mixer.music.load(rand)
				musicFile = rand
			else:
				#p = vlc.MediaPlayer("file:///home/pi/doorbell/audio/default.mp3")
                                mixer.music.load("/home/pi/doorbell/audio/default.mp3")
				musicFile = 'default.mp3'
			# Making sure that the they aren't disturbing my beauty sleep
			hour = int(strftime("%H"))
			weekday = datetime.date(int(strftime("%Y")),int(strftime("%m")),int(strftime("%d"))).weekday() # mo - su (0-6)
				# Mon - Thu 6 - 21											# Fri 6 - 22								# Saturday 8 - 22						# Sunday 8 - 21
			if (hour >= 6 and hour < 21 and weekday >= 0 and weekday <= 3 or hour >= 6 and hour < 22 and weekday == 4 or hour >= 8 and hour < 22 and weekday == 5 or hour >= 8 and hour < 21 and weekday == 6):
				# If the blue button is on, then don't disturb
				if (blue == False):
					#p.play()
                                        mixer.music.play()
					# Turning Green led on
					GPIO.output(5, True)
					musicPlaying = True
				else:
					print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'Not playing, blue is on\r'
			else:
				print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'Not playing, sleepy time\r'
			
			print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'Doorbell Pushed!\r'

			#Send pushover in a threading
			pushOverThread = FuncThread(PushOver, 'Doorbell','Doorbell pressed... Playing ' + musicFile)
			pushOverThread.start();
			# Updating amount of presses
			buttonPressAmount = 1;
			
			sleep(1)
		# Green button pressed while playing music
		elif (GPIO.input(6) == False and musicPlaying == True):
			print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'End button pressed\r'
			#p.stop()
                        mixer.music.stop()
			musicPlaying = False
			GPIO.output(5, False)
		# Music stopped by itself
		elif (musicPlaying == True and mixer.music.get_busy() == False):
			musicPlaying = False
			#p.stop()
                        mixer.music.stop()
			
			# Turning green led off
			GPIO.output(5, False)
			
			print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'Music file ended'
		elif (GPIO.input(18) == True and musicPlaying == True):
			buttonPressAmount = buttonPressAmount + 1
			print time.strftime("<%Y-%m-%d %H:%M:%S> ") + 'Asshole pressed again: ' + str(buttonPressAmount)
			sleep(1)
		sleep(0.1);
finally:
        GPIO.cleanup()
