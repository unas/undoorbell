import io
import time
import picamera
from PIL import Image

class Camera:
    def __init__(self):
        self.camera = picamera.PiCamera()

    def start_preview(self):
        self.camera.start_preview()

    def stop_preview(self):
        self.camera.stop_preview()

    def rotation(self, rotation):
        self.camera.rotation = rotation

    def get_jpg_stream(self):
        stream = io.BytesIO()
        self.camera.capture(stream, format='jpeg')
        stream.seek(0)
        return stream
