import peewee
import datetime
from configurationloader import ConfigurationLoader

config = ConfigurationLoader()

sqlConf = config.get_configuration()['mysql']

database = peewee.MySQLDatabase(sqlConf['database'],host=sqlConf['host'], user=sqlConf['user'], password=sqlConf['password'])

class LongBlogField(peewee.BlobField):
    field_type = 'LONGBLOB'

class Doorbell(peewee.Model):
    id = peewee.PrimaryKeyField(primary_key=True)
    date = peewee.DateTimeField(default=datetime.datetime.now)
    test = peewee.BooleanField(default=False)
    class Meta:
        database = database

class Picture(peewee.Model):
    id = peewee.PrimaryKeyField(primary_key=True)
    doorbell_id = peewee.ForeignKeyField(Doorbell, backref='pictures', to_field="id")
    file = LongBlogField()
    class Meta:
        database = database
