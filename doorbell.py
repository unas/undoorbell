import datetime
import pygame
import RPi.GPIO as GPIO
from picamera import PiCamera
import telegram
from doorbelllog import DoorbellLog
from camera import Camera
import os
import sys
import time
from threading import Thread
from dateutil.relativedelta import relativedelta
import random
from configurationloader import ConfigurationLoader
import logging

class Doorbell:
    def __init__(self):

        self.__config = ConfigurationLoader()
        
        self.__doorbell_button_pin = self.__config.get_configuration()['pins']['doorbell_button_pin']
        self.__blue_button_pin = self.__config.get_configuration()['pins']['blue_button_pin']
        self.__blue_led_pin = self.__config.get_configuration()['pins']['blue_led_pin']
        self.__red_button_pin = self.__config.get_configuration()['pins']['red_button_pin']
        self.__red_led_pin = self.__config.get_configuration()['pins']['red_led_pin']
        self.__green_button_pin = self.__config.get_configuration()['pins']['green_button_pin']
        self.__green_led_pin = self.__config.get_configuration()['pins']['green_led_pin']
        self.__uv_led_pin = self.__config.get_configuration()['pins']['uv_led_pin']
        
        self.__mute = False
        self.__uv_light_on = False
        self.__music_playing = False

        # Initialize pygame, needed to get camera image on screen
        pygame.init()

        self.__camera = Camera()
        self.__camera.rotation(self.__config.get_configuration()['camera']['rotation'])
        self.__camera.start_preview();
        
        self.__telegram_bot = telegram.Bot(token=self.__config.get_configuration()['telegram']['token'])
        self.__telegram_bot_chat_id = self.__config.get_configuration()['telegram']['chat_id']
        self.__doorbell_log = DoorbellLog(self.__config.get_configuration()['mysql'])
        self.__setup_gpio()

    def start_loop(self):
        while True:
            # Doorbell pressed
            if GPIO.input(self.__doorbell_button_pin) == True:
                logging.info("Doorbell pressed")
                self.__doorbell_button_callback()
            
            # Button to turn on IR light
            elif GPIO.input(self.__red_button_pin) == False:
                logging.info("Red pressed")
                self.__red_button_callback()
                time.sleep(0.3) # Bouncetime
            # Button to mute doorbell
            elif GPIO.input(self.__blue_button_pin) == False:
                logging.info("Blue pressed")
                self.__blue_button_callback()
                time.sleep(0.3) # Bouncetime
            # Button to stop doorbell music
            elif GPIO.input(self.__green_button_pin) == False:
                logging.info("Green pressed")
                self.__green_button_callback()
                time.sleep(0.3) # Bouncetime

            # If Doorbell was pressed a while ago and music stopped by itself
            if self.__music_playing and not pygame.mixer.music.get_busy():
                self.__music_playing = False
                GPIO.output(self.__green_led_pin, False)

            time.sleep(0.1)
    
    def __setup_gpio(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.__doorbell_button_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.__blue_button_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.__blue_led_pin, GPIO.OUT)
        GPIO.output(self.__blue_led_pin, False)
        GPIO.setup(self.__red_button_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.__red_led_pin, GPIO.OUT)
        GPIO.output(self.__red_led_pin, False)
        GPIO.setup(self.__green_button_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.__green_led_pin, GPIO.OUT)
        GPIO.output(self.__green_led_pin, False)
        GPIO.setup(self.__uv_led_pin, GPIO.OUT)
        GPIO.output(self.__uv_led_pin, False)
        
        # TODO Check if you can get this to work
        # This doesn't seem to work. This causes the buttons to be pressed by them selves every now and then.
        # I might be able to fix this buy rebuilding the hardware but that means I have to open my wall, so this is easier
        #GPIO.add_event_detect(self.__doorbell_button_pin, GPIO.RISING, callback=self.__doorbell_button_callback)
        #GPIO.add_event_detect(self.__blue_button_pin, GPIO.RISING, callback=self.__blue_button_callback)
        #GPIO.add_event_detect(self.__red_button_pin, GPIO.RISING, callback=self.__red_button_callback)
        #GPIO.add_event_detect(self.__green_button_pin, GPIO.RISING, callback=self.__green_button_callback)
    
    def __doorbell_button_callback(self):
        if not pygame.mixer.music.get_busy():
            now = datetime.datetime.now()
            current_date_time = now.strftime("%Y-%m-%d %H:%M:%S")
            self.__telegram_bot.send_message(chat_id=self.__telegram_bot_chat_id, text=f"Doorbell {current_date_time}")

            current_time = now.time()
            current_day = now.strftime("%A").lower()
            mute_start = datetime.time.fromisoformat(self.__config.get_configuration()['mute_times'][current_day]['start'])
            mute_end = datetime.time.fromisoformat(self.__config.get_configuration()['mute_times'][current_day]['end'])
            
            # Only playing song, if it isn't mute_time
            if mute_end <= current_time <= mute_start and self.__mute == False:
                self.__select_doorbell_music()
                pygame.mixer.music.play()
                self.__music_playing = True
                # Turning green led on
                GPIO.output(self.__green_led_pin, True)
            else:
                logging.info("Not playing song, mute")
            
            # Sending images to database and telegram in a new thread, so that the rest of the script won't wait for it to finish
            thread = Thread(target=self.__take_images_and_send)
            thread.start()
    
    def __select_doorbell_music(self):

        now = datetime.datetime.now()
        # Checking if the date matches with a special day from config
        custom_song = None
        custom_songs = self.__config.get_configuration()['audio']['custom_folder']
        path = os.path.abspath(os.path.dirname(sys.argv[0]))
        default_song_path = path + "/audio/" + self.__config.get_configuration()['audio']['default_file']

        for name in custom_songs:
            start_str = custom_songs[name]['date_start']
            start_date = datetime.datetime.strptime(start_str + "." + str(now.year), '%d.%m.%Y')
            end_str = custom_songs[name]['date_end']
            end_date = datetime.datetime.strptime(end_str + "." + str(now.year), '%d.%m.%Y')
            # Setting endDate time to end of day
            end_date = end_date.replace(hour=23, minute=59, second=59)
            # Checking if current date is between start and end
            if (start_date <= now or now <= end_date):
                # Getting a random song from custom list
                folder_path = path + '/audio/' + custom_songs[name]['folder']
                songs = os.listdir(folder_path)
                custom_song = folder_path + "/" + songs[random.randrange(len(songs))]
                break
        
        if custom_song is not None:
            logging.info("Selecting song: " + custom_song)
            pygame.mixer.music.load(custom_song)
        else:
            logging.info("Selecting default song: " + default_song_path)
            pygame.mixer.music.load(default_song_path)

    def __take_images_and_send(self):
        # Taking the image
        logging.info("Taking images")
        pictures = list()
        for n in range(self.__config.get_configuration()['camera']['pictures']):
            pic = self.__camera.get_jpg_stream()
            pictures.append(pic)
            time.sleep(1)
        
        telegram_media_list = [telegram.InputMediaPhoto(p) for p in pictures]
        logging.info("Sending images to telegram")
        self.__telegram_bot.send_media_group(chat_id=self.__telegram_bot_chat_id, media=telegram_media_list)
        
        # Saving to database
        logging.info("Saving images to the database")
        self.__doorbell_log.save_images(pictures)

        # Freeing memory
        logging.info("Freeing memory")
        for picture in pictures:
            picture.close

    def __blue_button_callback(self):
        self.__mute = not self.__mute
        GPIO.output(self.__blue_led_pin, self.__mute)
    
    def __red_button_callback(self):
        self.__uv_light_on = not self.__uv_light_on
        GPIO.output(self.__uv_led_pin, self.__uv_light_on)
        GPIO.output(self.__red_led_pin, self.__uv_light_on)
    
    def __green_button_callback(self):
        if pygame.mixer.music.get_busy():
            pygame.mixer.music.stop()
            # Turning green led off
            GPIO.output(self.__green_led_pin, False)
            self.__music_playing = False
