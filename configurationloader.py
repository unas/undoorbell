import yaml
import os
import sys

class ConfigurationLoader:
    def __init__(self):
        # Path, where this script is located
        self.__path = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"
        self.__configuration = self.__loadConfig()

    def __loadConfig(self):
        if not os.path.exists(self.__path + "config.yaml"):
            with open(self.__path + "default_config.yaml", "r") as default_f:
                with open(self.__path + "config.yaml", "w") as config_f:
                    config_f.write(default_f.read())
            print("config.yaml file not found. default_config.yaml was copied to config.yaml, please update the config file and run the program again")
            exit()

        with open(self.__path + "config.yaml", "r") as f:
            config = yaml.load(f, Loader=yaml.FullLoader)
            return config
    
    def get_configuration(self):
        return self.__configuration