#!/usr/bin/env python3

from doorbell import Doorbell
import RPi.GPIO as GPIO
import logging
import sys
import os

if __name__ == "__main__":

    script_folder_path = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

    logging.basicConfig(
        level = logging.INFO,
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        filename=script_folder_path + "doorbell.log"
    )

    try:
        logging.info("Starting...")
        doorbell = Doorbell()
        logging.info("Doorbell object created. Starting loop...")
        doorbell.start_loop()
    except KeyboardInterrupt:
        logging.info("Keyboard interrupt, exiting...")
    except:
        logging.exception("Doorbell crashed!")
        raise
    finally:
        GPIO.cleanup()
