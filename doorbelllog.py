from peewee import *
from databasemodels import Doorbell, Picture
import datetime

class DoorbellLog:
    def __init__(self, config):
        self.__Doorbell = Doorbell(config['host'], config['user'], config['password'], config['database'])
        self.__Picture = Picture(config['host'], config['user'], config['password'], config['database'])
        
        # Creating tables if they don't exist
        if not self.__Doorbell.table_exists():
            self.__Doorbell.create_table()
        if not self.__Picture.table_exists():
            self.__Picture.create_table()
    
    def save_images(self, images):
        with self.__Doorbell._meta.database.atomic():
            doorbell = self.__Doorbell.create(date=datetime.datetime.now())
            for image in images:
                self.__Picture.create(doorbell_id=doorbell.id, file=image.getvalue())
