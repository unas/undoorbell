# README #

Python3 script to work as a doorbell on a raspberry pi 3

### Setup ###
sudo apt install cargo build-essential libssl-dev libffi-dev python3-dev libsdl2-dev python3-pygame

pip3 install -r requirements.txt

sudo raspi-config
Enable camera

Get Telegram bot
- In telegram, talk to BotFather
    - /newbot
    - Choose name for bot
    - Choose username for bot
    - Copy token
- Add your bot to the telegram group you want to receive messages to
- Write anything to that group so that the bot can see it
- Open python3
    import telegram
    bot = telegram.Bot(token="<YOUR-TOKEN>")
    updates = bot.get_updates()
    print(updates[-1].message.chat_id)
- Copy the chat_id you got

### How to run ###
Run script
- python3 main.py

First time config.yaml is created, put own values

### Cron ###
sudo crontab -e
#add line, must run as user "pi", root can't play music with pygame.mixer
@reboot su - pi -c "/usr/bin/python3 /home/pi/doorbell2/main.py &"

### Camera ###
Disable camera led with
sudo vim /boot/config.txt
add line
disable_camera_led=1